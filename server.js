const express=require('express')
const mysql=require('mysql2')
const cors=require('cors')
const app=express()
app.use(cors('*'))
const srouter= require('./routes/student')
app.use('/student',srouter)
app.listen(1000,'0.0.0.0',()=>{
    console.log("server started at port no 1000")
})
module.exports=app