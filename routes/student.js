const { request, response } = require('express')
const mysql=require('mysql2')
const express=require('express')
const db=require('../db')
const utils=require('../utils')
const router =express.Router()
router.post('/',(request,response) =>{
    //student_id, s_name, password, course ,passing_year, prn_no, dob)  
    const {student_id, s_name, password, course ,passing_year, prn_no, dob}=request.body
    const query=`insert into student (student_id, s_name, password, course ,passing_year, prn_no, dob) values(?,?,?,?,?,?,?)`
    db.pool.execute(query,[student_id, s_name, password, course ,passing_year, prn_no, dob],(error,result)=>{
        response.send(utils.createResult(error,result))
        console.log(error)
    })
})
router.get('/:s_name',(request,response)=>{
    const {s_name}=request.params
    const query='select student_id, s_name, password, course ,passing_year, prn_no, dob from student where s_name=? '
    db.pool.execute(query,[s_name],(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})
 router.delete('/:dob',(request,response)=>{
     const {dob}=request.params
     const query='delete student where dob=? '
     db.pool.execute(query,[dob],(error,result)=>{
         response.send(utils.createResult(error,result))
     })
 })
 router.put('/:prn_no',(request,response)=>{
    const {dob}=request.params
    const query='update student set s_name=abc where prn_no=? '
    db.pool.execute(query,[dob],(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})
module.exports=router